import React, { useEffect, useState, createRef } from "react";
import {FLInput} from "../../components/wrapuikit";
import SuggestionList from "./dropdownsearch";
import SearchForm from "./searchform";
import {
  BrowserView,
  MobileView,
  isBrowser,
  isMobile,
} from "react-device-detect";
import {Button, Form} from "react-bootstrap";



const View = (props) => {
  const listInsur = [
    /*{
      name: lng.get("up8byhyx"),
      key: "BAY_AT",
    },*/
    {
      name: lng.get("skycare"),
      key:"SKY_CARE"
    },
    {
      name: lng.get("bycvqmd0"),
      key:"LOST_BAGGAGE"
    },
    /*{ QUYNHTM đóng lại theo issue: 337
      name: lng.get("skycaredom"),
      key:"SKY_CARE_DOMESTIC"
    },*/
  ];
  const searchformRef = createRef();
  const [loading, setLoading] = useState(false);
  const [focus, setFocus] = useState(false);
  const [openSugess, setOpenSuggess] = useState(false);
  //const [searchvalue, setSearchValue] = useState(lng.get('up8byhyx')); phutb comment
  const [searchvalue, setSearchValue] = useState(lng.get('skycare')); //bycvqmd0
  // const [searchvalue, setSearchValue] = useState("");
  //const [searchcode, setSeachCode] = useState("BAY_AT"); //phutb commment
  const [searchcode, setSeachCode] = useState("SKY_CARE"); //LOST_BAGGAGE
  // const [searchcode, setSeachCode] = useState(props.searchcode);

  const [isActive, setIsActive] = useState(0);

  useEffect(() => {
    setLoading(props.loading);
  }, [props.loading]);

  const onFocus = () => {
    setFocus(true);
    setOpenSuggess(true);
  };
  const onBlur = () => {
    if (searchvalue.length == 0 && isBrowser) {
      if (searchcode == null) {
        setFocus(false);
        setOpenSuggess(false);
      }
    }
  };
  const onSearchChange = (e) => {
    setSearchValue(e.target.value);
    if (e.target.value != searchvalue) {
      setSeachCode(null);
    }
  };
  const onCloseSearch = () => {
    setOpenSuggess(false);
    setSeachCode(null);
  };

  const iconClick = (e) => {
    setSearchValue("");
    setFocus(false);
    setOpenSuggess(false);
    setSeachCode(null);
    props.onClose();
    e.preventDefault();
    return e.stopPropagation();
  };
  const onItemSelect = (name, code) => {
    setSearchValue(name);
    setSeachCode(code);
    searchformRef.current.resetForm();
    props.onSearchCodeChange(name, code);
  };
  const setActiveItem = (vl, i) =>{
    setIsActive(i);
    setSearchValue(vl.name);
    setSeachCode(vl.key);
    searchformRef.current.resetForm();
    props.onSearchCodeChange(vl.name, vl.key);
  }
  return (
      <div className="vietjet-searchbar">
        <div className="hd-container">
          <div className="br-searchbar">
            <div className="row">
              <div className="col-12 col-lg-4 col-md-4 col-xs-12 list-insur">
                <p className="title-list-insur">{lng.get("ebdbrgpb")}</p>

                {listInsur.map((vl, i)=>{
                  return (
                      <p className={isActive === i ? "item-list-active" : "item-list-insur" } onClick={() =>setActiveItem(vl,i)} >
                        <i className="fa fa-shield-alt" />
                        <label>{vl.name}</label>
                        {isActive  === i ? <i className="far fa-hand-point-right" /> : null}
                      </p>
                  )
                })}
                {/*<div className="list-insur">*/}

                {/*  <p className="item-list-insur">*/}
                {/*    <i className="fa fa-shield-alt" />*/}
                {/*    <label>Bảo hiểm Hành lý</label>*/}
                {/*  </p>*/}
                {/*</div>*/}
              </div>
              <div className="col-12 col-lg-8 col-md-8 col-xs-12 form-search-list-insur">
                <label className="title-form-search">{searchvalue}</label>
                <SearchForm
                    ref={searchformRef}
                    loading={loading}
                    code={searchcode}
                    name={searchvalue}
                    onSearch={props.onSearch}
                />
              </div>
            </div>

            {/*<div className="search-animate">*/}
            {/*  <div className={focus ? "place-holder opeb" : "place-holder"}>*/}
            {/*    <div className="text">Chọn sản phẩm bảo hiểm cần tra cứu</div>*/}
            {/*    <div*/}
            {/*        className="icon-wrapper"*/}
            {/*        onClick={(e) => {*/}
            {/*          iconClick(e);*/}
            {/*        }}*/}
            {/*    >*/}
            {/*      <span className={focus ? "magic-icon-search close" : "magic-icon-search"}/>*/}
            {/*    </div>*/}
            {/*  </div>*/}
            {/*  {focus && searchvalue == null && (*/}
            {/*    <div onClick={() => onBlur()} className="outside-search" />*/}
            {/*  )}*/}
            {/*  <input*/}
            {/*    disabled={searchcode != null}*/}
            {/*    type="text"*/}
            {/*    onFocus={onFocus}*/}
            {/*    onChange={onSearchChange}*/}
            {/*    value={searchvalue}*/}
            {/*  />*/}
            {/*</div>*/}

            {/*<SearchForm*/}
            {/*  ref={searchformRef}*/}
            {/*  loading={loading}*/}
            {/*  code={searchcode}*/}
            {/*  onSearch={props.onSearch}*/}
            {/*/>*/}
          </div>


          {/*{searchcode == null ? (*/}
          {/*  <SuggestionList*/}
          {/*    show={openSugess}*/}
          {/*    searchvalue={searchvalue}*/}
          {/*    onItemSelect={onItemSelect}*/}
          {/*    onSearchChange={onSearchChange}*/}
          {/*    onCloseSearch={onCloseSearch}*/}
          {/*  />*/}
          {/*) : null}*/}
        </div>
      </div>
  );
};

export default View;
