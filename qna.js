import React, { useEffect, useState } from "react";
import QnA from "../../components/qna";

const View = (props) => {
    const [title, setTitle] = useState(lng.get('skycareTitle'));//ej68sgd9
    const [idQna, setIdQna] = useState("642e43de7baa8f1084c26206"); //60fe7725c3c0100fadb4bd9e
    useEffect(() =>{
        switch (props.searchcode){
            case "SKY_CARE":
                setTitle(lng.get('skycareTitle'));
                setIdQna("642e43de7baa8f1084c26206");
                break;
            case "BAY_AT":
                setTitle(lng.get('ej68sgd9'));
                setIdQna("60fe7725c3c0100fadb4bd9e");
                break;
            case "SKY_CARE_DOMESTIC":
                setTitle(lng.get('skycaredomTitle'));
                setIdQna("64940659b30d4810ade8749f");
                break;
            case "LOST_BAGGAGE":
                setTitle(lng.get("l3qscdhs"));
                setIdQna("60fe7785c3c0100fadb4bda3");
                break;
        }
    },[props.searchcode])

  return (
    <>
        {props.searchcode && (
            <QnA title={title} idQna={idQna}/>
        )}
    </>
  );
};

export default View;
