import React, { useEffect, useState } from "react";
import TopBanner from "../../components/topbanner";


var mobile = require('is-mobile');

var backgroundDesktop = "/img/vietjet/ATB.jpg"
var backgroundMobile = "/img/vietjet/bannermobile.png"



const View = (props) => {
  
  const [background, setBackground] = useState("/img/vietjet/ATB.jpg")
  useEffect(() => {
      setBackground(mobile()?backgroundMobile:backgroundDesktop)
  }, [mobile()]);



  return (
    <div>
      <TopBanner className="vietjet">
        <div
          className="topbanner-vietjet"
          style={{ backgroundImage: `url(${background})` }}
        >
          <div className="top-content">
            <h1 lng={"ricskak2"}>{lng.get('ricskak2')}</h1>
            {/* <p>
              Tìm kiếm, tra cứu thông tin hợp đồng của Bảo hiểm HD hợp tác với
              Vietjet Air
            </p> */}
            {/* <a href="https://hdinsurance.com.vn">
              <Button className="btn-style3">Tìm hiểu Bảo hiểm HD</Button>
            </a> */}
          </div>
        </div>
      </TopBanner>
    </div>
  );
};

export default View;
