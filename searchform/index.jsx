import React, {
  useEffect,
  useState,
  createRef,
  useImperativeHandle,
  forwardRef,
} from "react";
import { Button, Form } from "react-bootstrap";
import {
  FLInput,
  FLDate,
  FLSelect,
  FLSelectFlight,
  FLAirport,
} from "../../../components/wrapuikit";

import api from "../../../services/Network";
import ReactHtmlParser from "react-html-parser";
import moment from "moment";
import Language from "../../../services/Lang";

const View = forwardRef((props, ref) => {
  const formRef = createRef();
  const [loading, setLoading] = useState(false);

  const [validated, setValidated] = useState(false);

  // const [name, setName] = useState(null);
  const [lastName, setLastName] = useState(null);
  const [midAndDivenName, setMidAndGivenName] = useState(null);
  const [bookingCode, setBookingCode] = useState(null);
  const [seatNo, setSeatNo] = useState(null);
  const [flightCode, setFlightCode] = useState(null);
  const [flightDate, setFlightDate] = useState(null);
  const [phoneNumber, setPhoneNumber] = useState(null);

  const [linkGuide, setLinkGuide] = useState('');

  const [go_from, setGoFrom] = useState({
    label: "",
    country: null,
    airport: null,
  });
  const [go_to, setGoTo] = useState({
    label: "",
    country: null,
    airport: null,
  });

  // const [code, setCode] = useState(null);
  
  const [code, setCode] = useState('SKY_CARE');

  const [searchMode, setSearchMode] = useState(0);

  const [flightList, setFlightList] = useState([]);

  const [enbFlightSchedule, setEnbFlightSchedule] = useState(false);

  useImperativeHandle(ref, () => ({
    resetForm() {
      setLastName("");
      setMidAndGivenName("");
      setBookingCode("");
      setSeatNo("");
      setFlightCode(null);
      setFlightDate("");
    },
  }));

  useEffect(() => {
    /*if (props.code === 'BAY_AT') {
      setLinkGuide('link_portal_guide')
    } else */
    /*else if (props.code === 'SKY_CARE_DOMESTIC') {
      setLinkGuide('link_sky_care_dom_guide')
    }*/
    if (props.code === 'SKY_CARE'|| props.code===null) {
      setLinkGuide('link_sky_care_guide')
    } else {
      setLinkGuide('lost_baggage_guide')
    }
    setCode(props.code);
    getListFlightSchedule();

  }, [props.code]);

  useEffect(() => {
    if (searchMode == 1) {
      if (go_from.airport && go_to.airport && flightDate) {
        getListFlightSchedule(go_from, go_to, flightDate);
        setEnbFlightSchedule(true);
      }
    }
  }, [go_from, go_to, flightDate]);

  useEffect(() => {
    setLoading(props.loading);
  }, [props.loading]);

  const removeAccents = (str) => {
    var AccentsMap = [
      "aàảãáạăằẳẵắặâầẩẫấậ",
      "AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ",
      "dđ",
      "DĐ",
      "eèẻẽéẹêềểễếệ",
      "EÈẺẼÉẸÊỀỂỄẾỆ",
      "iìỉĩíị",
      "IÌỈĨÍỊ",
      "oòỏõóọôồổỗốộơờởỡớợ",
      "OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ",
      "uùủũúụưừửữứự",
      "UÙỦŨÚỤƯỪỬỮỨỰ",
      "yỳỷỹýỵ",
      "YỲỶỸÝỴ",
    ];
    for (var i = 0; i < AccentsMap.length; i++) {
      var re = new RegExp("[" + AccentsMap[i].substr(1) + "]", "g");
      var char = AccentsMap[i][0];
      str = str.replace(re, char);
    }
    return str;
  };

  const getListFlightSchedule = async (_from, _to, date) => {
    try {
      const data = {
        p_project: "",
        p_username: "",
        p_org_code: "VIETJET_VN",
        p_flidate: date,
        p_departure: _from.airport,
        p_arrival: _to.airport,
      };
      const response = await api.post("/api/get-vj-flight", data);
      if (response.data[0]) {
        setFlightList(response.data[0]);
      }
    } catch (e) {}
  };

  const setNameValue = (value) => {
    setName(value);
  };
  const setBookingValue = (value) => {
    setBookingCode(value);
  };
  const setFlightValue = (value) => {
    console.log(value);
    setFlightCode(value);
  };
  const setSeatNoValue = (value) => {
    setSeatNo(value);
  };

  const selectedDay = () => {
    const tdate = moment();
    return {
      year: tdate.year(),
      month: tdate.month() + 1,
      day: tdate.date(),
    };
  };

  const onCheckedChange = (e) => {
    if (e.target.checked) {
      setSearchMode(e.target.value);
      setSeatNo("");
      setBookingCode("");
      setFlightCode("");
    }
  };

  const submitSearchForm = () => {
    if (!loading) {
      const form = formRef.current;
      if (form.checkValidity() === false) {
        setValidated(true);
        return false;
      }
      if (searchMode == 0) {
        props.onSearch({
          a1: new Language().getLang(),
          a2: bookingCode ? bookingCode.toUpperCase() : null,
          a3: removeAccents(lastName.trim() + " " + midAndDivenName.trim())
              .toUpperCase()
              .trim(),
          a4: phoneNumber ? phoneNumber : null,
          a7: flightDate ? flightDate : null,
          a8: bookingCode ?bookingCode.toUpperCase() : null,
          a9: seatNo ? seatNo.toUpperCase() : "",
          a12: flightCode ? flightCode : null,
        });
      } else {
        props.onSearch({
          // a2: bookingCode ? bookingCode.toUpperCase() : null,
          a1: new Language().getLang(),
          a3: removeAccents(lastName.trim() + " " + midAndDivenName)
              .toUpperCase()
              .trim(),
          a7: flightDate,
          a8: bookingCode.toUpperCase(),
          a9: seatNo.toUpperCase(),
          a10: go_from.airport,
          a11: go_to.airport,
          a12: flightCode ? flightCode : null,
        });
      }
      return false;
    }
  };

  const flySafeInsur = () =>{
    return (
        <div className="type-form-1">
          <div className="row">
            <div className="col-md-4 mt15">
              <div className="group-ipnv">
                <FLInput
                    disable={false}
                    loading={loading}
                    value={lastName}
                    changeEvent={(v) => setLastName(v)}
                    isUpperCase={true}
                    placeholder="NGUYEN"
                    label={lng.get("lkkie8tq")}
                    hideborder={true}
                    required={true}
                />
                <div className="ver-line"></div>
                <div className="kkjs">
                  <FLInput
                      disable={false}
                      loading={loading}
                      value={midAndDivenName}
                      changeEvent={(v) => setMidAndGivenName(v)}
                      isUpperCase={true}
                      hideborder={true}
                      placeholder="VAN A"
                      label={lng.get("deyolsek")}
                      required={true}
                  />
                </div>
              </div>
            </div>
            <div className="col-md-4 col-6 mt15">
              <FLDate
                  loading={loading}
                  disable={false}
                  value={flightDate}
                  changeEvent={setFlightDate}
                  selectedDay={selectedDay()}
                  label={lng.get("5rt9iig8")}
                  required={true}
              />
            </div>
            <div className="col-md-4 col-6 mt15">
              <FLInput
                  loading={loading}
                  disable={searchMode == 1}
                  value={bookingCode}
                  changeEvent={setBookingValue}
                  isUpperCase={true}
                  label={lng.get("n5tcm1pu")}
                  required={searchMode == 0}
              />
            </div>

            <div className="col-md-4 mt15">
              <FLInput
                  disable={false}
                  value={flightCode}
                  changeEvent={setFlightValue}
                  isUpperCase={true}
                  loading={loading}
                  label={lng.get("tceokqgh")}
                  required={false}
              />
            </div>

            <div className="col-md-4 mt15">
              <FLInput
                  disable={false}
                  value={seatNo}
                  changeEvent={setSeatNoValue}
                  isUpperCase={true}
                  loading={loading}
                  label={lng.get("90nhfruf")}
                  required={false}
              />
            </div>
          </div>
        </div>
    )
  }

  const lostBaggage = () =>{
    return(
        <div className="type-form-1">
          <div className="row">
            <div className="col-md-4 mt15">
              <div className="group-ipnv">
                <FLInput
                    disable={false}
                    loading={loading}
                    value={lastName}
                    changeEvent={(v) => setLastName(v)}
                    isUpperCase={true}
                    placeholder="NGUYEN"
                    label={lng.get("lkkie8tq")}
                    hideborder={true}
                    required={true}
                />
                <div className="ver-line"></div>
                <div className="kkjs">
                  <FLInput
                      disable={false}
                      loading={loading}
                      value={midAndDivenName}
                      changeEvent={(v) => setMidAndGivenName(v)}
                      isUpperCase={true}
                      hideborder={true}
                      placeholder="VAN A"
                      label={lng.get("deyolsek")}
                      required={true}
                  />
                </div>
              </div>
            </div>
            <div className="col-md-4 col-6 mt15">
              <FLInput
                  loading={loading}
                  // disable={searchMode == 1}
                  value={phoneNumber}
                  changeEvent={setPhoneNumber}
                  // isUpperCase={true}
                  label={lng.get("dlii8f7l")}
                  required={true}
              />
            </div>

            <div className="col-md-4 mt15">
              <FLInput
                  disable={false}
                  value={bookingCode}
                  changeEvent={setBookingCode}
                  // isUpperCase={true}
                  loading={loading}
                  label={lng.get('c5siztek')}
                  required={true}
              />
            </div>
            <div className="col-md-4 mt15">
              <FLDate
                  loading={loading}
                  disable={false}
                  value={flightDate}
                  changeEvent={setFlightDate}
                  selectedDay={selectedDay()}
                  label={lng.get("5rt9iig8")}
                  required={false}
              />
            </div>
          </div>
        </div>
    )
  }

  return (
      <div className={code ? "searchform show" : "searchform"}>
        <div className="form-container">
          <Form
              ref={formRef}
              className="search-form-input"
              noValidate
              validated={validated}
          >
            {/* {(props.code ===null||props.code ===''||props.code === 'BAY_AT' || props.code === 'SKY_CARE' || props.code === 'SKY_CARE_DOMESTIC') ? flySafeInsur() : lostBaggage()} */}
            {(props.code === 'BAY_AT' || props.code === 'SKY_CARE' || props.code === 'SKY_CARE_DOMESTIC') ? flySafeInsur() : lostBaggage()}
          </Form>
        </div>

        <div className="search-footer">
          <Button
              type="submit"
              className="btn-search"
              onClick={() => submitSearchForm()}
              disabled={loading}
          >
            {loading ? (
                <div>{l.g("searchform.button")}</div>
            ) : (
                <div lng="rducp5x2">
                  <i class="fa fa-search" aria-hidden="true"></i>
                  {lng.get("rducp5x2")}
                </div>
            )}
          </Button>
          <div className="">
            <a
                className="guide-infor"
                target="_blank"
                lng="link_portal_guide"
                href={ReactHtmlParser(lng.get(linkGuide))}
            >
              {lng.get("portal_guidance")}
            </a>
            <br></br>
              <i style={{color: 'rgb(238, 29, 35)', fontSize: '14px'}}>{lng.get("searching_note")}</i>
          </div>
        </div>
      </div>
  );
});

export default View;
