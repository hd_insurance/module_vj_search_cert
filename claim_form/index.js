import React, {useEffect} from "react";
import { ClaimFormVj, ClaimLostBaggage } from "../../../sdk/index.js";


const Index = (props) => {

  const showClaimForm = () =>{
    switch (props.outerRouter.query.prod) {
      /*case "BAY_AT":
        return <ClaimFormVj {...props} />;*/
      case "LOST_BAGGAGE":
        return <ClaimLostBaggage {...props} />;

    }
  }
  return showClaimForm();
};

export default Index;
