import React, { useEffect, useState } from "react";
import {FLInput} from "../../components/wrapuikit";
import {
  BrowserView,
  MobileView,
  isBrowser,
  isMobile,
} from "react-device-detect";
import Sheet from "react-modal-sheet";

import { Button } from "react-bootstrap";



const removeAccents = (str) => {
  var AccentsMap = [
    "aàảãáạăằẳẵắặâầẩẫấậ",
    "AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ",
    "dđ",
    "DĐ",
    "eèẻẽéẹêềểễếệ",
    "EÈẺẼÉẸÊỀỂỄẾỆ",
    "iìỉĩíị",
    "IÌỈĨÍỊ",
    "oòỏõóọôồổỗốộơờởỡớợ",
    "OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ",
    "uùủũúụưừửữứự",
    "UÙỦŨÚỤƯỪỬỮỨỰ",
    "yỳỷỹýỵ",
    "YỲỶỸÝỴ",
  ];
  for (var i = 0; i < AccentsMap.length; i++) {
    var re = new RegExp("[" + AccentsMap[i].substr(1) + "]", "g");
    var char = AccentsMap[i][0];
    str = str.replace(re, char);
  }
  return str;
};

const filterFunction = (name, value) => {
  const l1 = removeAccents(name.toString()).toLowerCase();
  const l2 = removeAccents(value).toLowerCase();
  return l1.includes(l2);
};

const View = (props) => {
  const data = [
    {
      //end: Bay an toan Insurance
      name: l.g('searchform.search_suggestion_BAYAT'),
      key: "BAY_AT",
    },
    {
      name:"Bảo hiểm hành lý",
      key:"LOST_BAGGAGE"
    },
    // {
    //   name: "Bảo hiểm Về nhà An tâm",
    //   key: "VNAT",
    // },
  ];
  const [show, setShow] = useState(false);
  const [listResult, setListResult] = useState(data);
  const [value, setValue] = useState("");

  useEffect(() => {
    setShow(props.show);
  }, [props.show]);

  useEffect(() => {
    setValue(props.searchvalue);
    var filtered = data.filter((obx) => {
      const vl = obx.name;
      return filterFunction(vl, props.searchvalue);
    });
    setListResult(filtered);
  }, [props.searchvalue]);

  const onCloseSearch = () => {
    props.onCloseSearch();
  };
  const onFocus = () => {
    setFocus(true);
  };
  const onBlur = () => {
    setFocus(false);
  };

  const onMobileInputChange = (e) => {
    setValue(e.target.value);
  };

  const onItemSelect = (name, code, e) => {
    props.onItemSelect(name, code);
    e.preventDefault();
    return e.stopPropagation();
  };

  if (isMobile) {
    return (
      <Sheet snapPoints={[600]} isOpen={show} onClose={() => onCloseSearch()}>
        <Sheet.Container>
          <Sheet.Header>
            <div className="vj-header-search">
              <div className="btn-close" onClick={() => onCloseSearch()}>
                <i class="fas fa-times"></i>
              </div>
              <h4>Chọn sản phẩm bảo hiểm</h4>
              <p>Nhập để tìm kiếm</p>
            </div>
          </Sheet.Header>
          <Sheet.Content>
            <div className="mobile-search-f">
              <div className="input-search-mb">
                <input
                  type="text"
                  placeholder={"Tìm kiếm"}
                  onChange={props.onSearchChange}
                  value={value}
                />
                <i class="fas fa-search"></i>
              </div>
            </div>
            <div className="list-vj-item-search">
              <ul>
                {listResult.map((item, index) => {
                  return (
                    <li
                      onClick={(e) => onItemSelect(item.name, item.key, e)}
                      key={index}
                    >
                      <i class="fas fa-shield-alt"></i>
                      <span> {item.name}</span>
                    </li>
                  );
                })}
              </ul>
            </div>
          </Sheet.Content>
        </Sheet.Container>

        <Sheet.Backdrop />
      </Sheet>
    );
  } else {
    return (
      <div
        className={
          show
            ? "vietjet-searchbar-suggestion"
            : "vietjet-searchbar-suggestion hide"
        }
      >
        <ul>
          {listResult.map((item, index) => {
            return (
              <li
                onClick={(e) => onItemSelect(item.name, item.key, e)}
                key={index}
              >
                <i class="fas fa-shield-alt"></i>
                <span> {item.name}</span>
              </li>
            );
          })}
          {listResult.length == 0 && (
            <div className="noresult-vj">Không có kết quả</div>
          )}
        </ul>
      </div>
    );
  }
};

export default View;
