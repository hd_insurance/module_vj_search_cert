import React, { useEffect, useState, createRef, useRef } from "react";
import TopBanner from "./top-banner.jsx";
import QnA from "./qna.js";
import SearchBar from "./searchbar.jsx";
// import NeedSuport from "../../needsuport";
// import RegisterForm from "../../registerform";
// import ProductList from "../../products/list";
import CardHolderInfo from "../../components/cardholderinfo/vietjetresult";

import ShowItemInfo from "../../components/cardholderinfo/mutilItemVietjet";

// import DocumentDownload from "./document-dowload";
import api from "../../services/Network";
import {Button, Form, Modal} from "react-bootstrap";
import {FLInput} from "hdi-uikit";



var searchbodyRef = null;


const View = (props) => {
  const [listCertInfo, setListcertInfo] = useState([]);
  const [certInfo, setCertInfo] = useState(null);
  const [claimInfo, setClaimInfo] = useState(null);
  const [loading, setLoading] = useState(false);
  const [emptyResult, setEmptyResult] = useState(false);
  const [searchcode, setSeachCode] = useState("SKY_CARE");//LOST_BAGGAGE

  const [showModalGcn, setShowModalGcn] = useState(false);
  // const [searchcode, setSeachCode] = useState(null);


  const onSearchClick = (params) => {
    search(params);
  };
  const onSearchCodeChange = (name, code) => {
    setSeachCode(code);
  };
  const onCloseSearch = () => {
    setEmptyResult(false);
    setCertInfo(null);
  };
  const search = async (param) => {
    let pathUrl = '';
    try {
      setLoading(true);
      setCertInfo(null);
      setClaimInfo(null)
      setEmptyResult(false);
      console.log('searchcode', searchcode);
      //QuynhTM đóng lại ko dùng BAY_AT if(searchcode === 'BAY_AT' || searchcode === 'SKY_CARE' || searchcode === 'SKY_CARE_DOMESTIC'){
      //if(searchcode === 'SKY_CARE' || searchcode === 'SKY_CARE_DOMESTIC'){
      if(searchcode === 'SKY_CARE'){
        pathUrl = `/api/vj/search-insur-info/${searchcode}`
      }else{
        pathUrl = `/api/vj/get-search-insur-info/${searchcode}`
      }
      const response = await api.post(pathUrl, param);
      setLoading(false);
      if (searchbodyRef) {
        searchbodyRef.scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "nearest",
        });
      }

      if (response.data[0]) {
        // if (response.data[0][0]) {
        //   const cert = response.data[0][0];
        //   console.log('cert', cert);
        //   console.log('setClaimInfo',response.data[1]);
        //   if(response.data[1][0]){
        //     setClaimInfo(response.data[1]);
        //   }
        //   setCertInfo(cert);
        //   setEmptyResult(false);
        // } else {
        //   setEmptyResult(true);
        // }
        if(response.data[0].length > 1){
          setListcertInfo(response.data[0]);
          setShowModalGcn(true);
        }else{
          if (response.data[0][0]) {
            const cert = response.data[0][0];
            setCertInfo(cert);
            setEmptyResult(false);
          } else {
            setEmptyResult(true);
          }
        }
        if(response.data[1][0]){
          setClaimInfo(response.data[1]);
        }
      } else {
        setEmptyResult(true);
      }
    } catch (e) {
      console.log(e);
      setEmptyResult(true);
    }
  };

  const previewGCN = (params) =>{
    setCertInfo(listCertInfo[params]);
    setShowModalGcn(false);
  }


  return (
    <div>
      <TopBanner />
      <SearchBar
        loading={loading}
        searchcode={searchcode}
        onSearch={onSearchClick}
        onClose={onCloseSearch}
        onSearchCodeChange={onSearchCodeChange}
      />
      <div
        className="search-body"
        ref={(ref) => {
          searchbodyRef = ref;
        }}
      >
        {certInfo && emptyResult == false && (
          <CardHolderInfo certInfo={certInfo} claimInfo={claimInfo} searchcode={searchcode} />
        )}
        {emptyResult && (
          <div className="hd-container search-not-found">
            <h1>{lng.get("not_found_result")}</h1>
            <img src="/img/vietjet/search-not-found.png" />
          </div>
        )}
      </div>
      {/* <ProductList/> */}
      {/* <NeedSuport/> */}
      {/* <DocumentDownload /> */}
      <QnA searchcode={searchcode} />
      {/* <AppBanner /> */}

      <Modal
          show={showModalGcn}
          onHide={() =>setShowModalGcn(false)}
          backdrop="static"
          keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>Chọn giấy chứng nhận</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{fontSize: 14}}>
          <label>Chọn giấy chứng nhận bạn muốn xem</label>
          <ShowItemInfo listCertInfo={listCertInfo} previewGCN={previewGCN} />
        </Modal.Body>
      </Modal>

    </div>
  );
};

export default View;
