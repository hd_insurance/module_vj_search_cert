import React, { useEffect, useState, createRef } from "react";
import {Col, Row} from "react-bootstrap";
const ViewDocumentDown = (props) => {
    const [docs, setDocs] = useState({})
    useEffect(() => {
        try {
            if(props.listPackage){
                if(props.listPackage.length > 0){
                    const doc = {
                        URL_BENE: props.listPackage[0].URL_BENE,
                        URL_RULE: props.listPackage[0].URL_RULE,
                        URL_GYC: props.listPackage[0].URL_GYC,
                        URL_BV: props.listPackage[0].URL_BV,
                        URL_BT: props.listPackage[0].URL_BT
                    }
                    setDocs(doc)
                }
            }
           
        } catch (e) {
        }
    }, [props.listPackage]);

    return(
        <div className="document-down policy-document">
            <h1>{l.g('bhsk.landing.document_title')}</h1>
            <div className="hd-container view-positins">
                <Row style={{justifyContent: 'center'}}>
                    <Col md={4}>
                        <div className="item-document">
                            <i className="fas fa-file-alt"></i>
                            {l.g('bhsk.landing.docs1')}
                           <a target="_blank" href={`https://hyperservices.hdinsurance.com.vn${docs.URL_BENE}?download=true`}><label>({l.g('bhsk.landing.download_docs')})</label></a>
                        </div>
                    </Col>
                    <Col md={4}>
                        <div className="item-document">
                            <i className="fas fa-file-alt"></i>
                            {l.g('bhsk.landing.docs5')}
                            <a target="_blank" href={`https://hyperservices.hdinsurance.com.vn${docs.URL_RULE}?download=true`}><label>({l.g('bhsk.landing.download_docs')})</label></a>
                        </div>
                    </Col>
                    <Col md={4}>
                        <div className="item-document">
                            <i className="fas fa-file-alt"></i>
                           {l.g('bhsk.landing.docs2')}
                            <a target="_blank" href={`https://hyperservices.hdinsurance.com.vn${docs.URL_GYC}?download=true`}><label>({l.g('bhsk.landing.download_docs')})</label></a>
                        </div>
                    </Col>
                </Row>
                <Row style={{justifyContent: 'center',}}>
                    <Col md={4}>
                        <div className="item-document">
                            <i className="fas fa-file-alt"></i>
                           {l.g('bhsk.landing.docs3')}
                            <a target="_blank" href={`https://hyperservices.hdinsurance.com.vn${docs.URL_BV}?download=true`}><label>({l.g('bhsk.landing.download_docs')})</label></a>
                        </div>
                    </Col>
                    <Col md={4}>
                        <div className="item-document">
                            <i className="fas fa-file-alt"></i>
                           {l.g('bhsk.landing.docs4')}
                            <a target="_blank" href={`https://hyperservices.hdinsurance.com.vn${docs.URL_BT}?download=true`}><label>({l.g('bhsk.landing.download_docs')})</label></a>
                        </div>
                    </Col>
                </Row>
            </div>
            {/* <img className="img-bg-dc" src="/img/img_bg_document.png" /> */}
        </div>
    )};

export default ViewDocumentDown;
